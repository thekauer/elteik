# Dimat 2
- [Dimat 2](#dimat-2)
  - [Kombinatorika](#kombinatorika)
    - [példák (minta zh 1.feladat):](#példák-minta-zh-1feladat)
    - [bonyolultabb példák (mintazh 2. feladat)](#bonyolultabb-példák-mintazh-2-feladat)
    - [Szita elv](#szita-elv)
    - [példa](#példa)
  - [Binomiális tétel](#binomiális-tétel)
    - [példa:](#példa-1)
  - [Gráfok](#gráfok)
    - [(Irányítatlan) Gráf](#irányítatlan-gráf)
    - [példák](#példák)
    - [Egy **vonal** a gráf **Euler-vonala** ha a gráf minden élét tartalmazza, lehet zárt vagy nyílt( a ceruza felemelése nélkül le lehet rajzolni a gráfot).](#egy-vonal-a-gráf-euler-vonala-ha-a-gráf-minden-élét-tartalmazza-lehet-zárt-vagy-nyílt-a-ceruza-felemelése-nélkül-le-lehet-rajzolni-a-gráfot)
    - [**Zárt Euler-vonal** ha a gráf minden csúcsának fokszáma **páros**.](#zárt-euler-vonal-ha-a-gráf-minden-csúcsának-fokszáma-páros)
    - [**Nyílt Euler-vonal** ha a gráfnak **két paratlan** és **n-2 páros** fokszámú csúcsa](#nyílt-euler-vonal-ha-a-gráfnak-két-paratlan-és-n-2-páros-fokszámú-csúcsa)
## Kombinatorika
|                                              | minden különböző | vannak egyformák      |
| -------------------------------------------- | ---------------- | --------------------- |
| **mindent sorba rakunk**                     | permutáció       | ismétléses permutáció |
| **néhányat választunk, nem számt a sorrend** | kombináció       | ismétléses kombináció |
| **néhányat választunk, számít a sorrend**    | variáció         | ismétléses variáció   |

1. **permutáció**
$$n!$$
2. **ismétléses permutáció**
$$\frac{n!}{n_1!n_2!\;...\;n_k!}$$
3. ** kombináció**
$$\binom{n}{k}=\frac{n!}{k!(n-k)!}$$
4. **istmétléses kombináció**
$$\binom{n+r-1}{r} $$
5. **variáció**
$$\frac{n!}{(n-k)!} = n \cdot(n-1)\cdot (n-2) \cdot\; ... \;\cdot(n-k)$$
6. **ismétléses variáció**
$$ n^k $$
7. **"kerekkasztal" permutáció**
$$\frac{n!}{n}$$

### példák (minta zh 1.feladat):
* (a). hányféleképpen lehet sorba rakni 5 kék, 4 piros 1 fehér golyót
mindent golyót sorba rakunk de vannak egyforma gólyók, tehát ez egy **ismétléses permutáció**.
$$
\frac{(5+4+1)!}{5!4!1!}
$$
* (b). Egy  fagyiz ́oban  **7-féle**  fagyit   ́árulnak.Hányfáleképpen  vehetünk  3  gombócos  fagyit  (a gombócokat  egymásra  pakolják  a  tölcsérben,  a  tölcséren  belüli  gombócok  **sorrendje  nemszámít**)?
7 féle fagyi van de csak néhányat választunk, és nem számít a sorrendjük ezért ez egy **ismétléses  variáció**
$$
7^3
$$
mert az első fagyinak is 7 félét választhatok, a másodiknak is és a 3.nak is.

* (c).Hányféleképpen lehet 10 tanulóközött 4 **különböző** könyvet kiosztani, ha mindegyikük **legfeljebb egy** könyvet kaphat?
10 diák van de nem mindeggyikük fog könyvet kapni, minden diák csak 1 könyvet kaphat tehát **nincs ismétlés**,mindegy hogy milyen sorrendben adjuk a könyveket. Ezért ez egy **variáció**
$\frac{10!}{(10-4)!}=10\cdot 9\cdot 8 \cdot 7$.
Mert az első könyvet 10 diák között osztom ki, a másodikat már csak 9 között mert az első már nem kaphat, a 3,-at 8 között, a negyedik pedig már csak 7 közt.
* (d). 25-ször feldobunk egy dobókockát.  Hányféle dobássorozat alakulhat ki?
A dobások **sorrendje számít** és **ismételjük** a dobásokat, egy dobásnak 6 féle eredméyne lehet.
$$
6^{25}
$$
Először dobhatok 6 félét, másodjára is hatfélét, és így tovább 25. dobásig.

* (e). Egy 32-lapos kártyacsomagból 8 lapot húzunk.  Hányfáleképpen alakulhat a húzás eredményeha a kihúzott lapok **sorrendje nem számít**?
8 lapot **kiválasztunk** és a **sorrend nem számít**
$$
\binom{32}{8}
$$
* (f). ́Hányféleképpen  ülhet  le  7  ember  egy  **kerekasztal**  köré?   (a  forgatással  egymásba   ́átvihető ültetések azonosnak számítanak)
$$
\frac{7!}{7}
$$

### bonyolultabb példák (mintazh 2. feladat)
* (a).Az 52-lapos francia kártyában 4  ́ász és 4 király van.  Osztunk  ́úgy, hogy 4 ját ́ékosnak 10-10lapot adunk.  Hányféle olyan szétosztás lehetséges, melyek során a 4 ját ́ékos mindegyikének 1-1  ́ász  ́es 1-1 király jut, ha a játékosok sorrendjét megkülönböztetjük (azaz számıt az, hogykinek osztunk először, másodszor stb)
4 játékos van **mindenkinek osztunk** és **számít a sorrend** (**permutáció**)
$$
4! \cdot
$$
Minendenkinek osztunk 8 lapot. Amihez mindig kiválasztunk a maradék lapokból 8-at(**kombináció**). Az adott hogy kiosztunk 4 ászt és 4 királyt ami összesen 8 lap. Ezért már csak 44 lap marad amiből tudunk választani a játékosoknak.
$$
4! \cdot \binom{44}{8} \cdot \binom{36}{8} \cdot \binom{28}{8} \cdot \binom{20}{8} \cdot
$$
Minden játékosnak **kiválasztunk** a 4 ászból eggyet, és a 4 királyból is eggyet.(**kombináció**)
$$
4! \cdot \binom{44}{8} \cdot \binom{36}{8} \cdot \binom{28}{8} \cdot \binom{20}{8} \cdot \binom{4}{1} \cdot \binom{4}{1}
$$
És ha nektek is ködös, hogy példában miért 4 el szoroztuk meg a királyoknál és az ászoknál, azért mert $\binom{4}{1}=4$.

* (b). Egy gyárban egy műszak alatt elkészített 200 db termék 5%-a selejtes.  Hányféleképpen lehetkiv ́alasztani 15 terméket úgy, hogy a kiválasztottak közül legalább 2 selejtes legyen?
Az 5% selejtes azt jelenti, hogy 
$200 \cdot (1-0,05) = 200 \cdot 0,95 = 190$ jó és
$200-190 = 10$ selejtes van.
Ezt a feladtott úgy érdmes megoldani, hogy az összes esetből kivonjuk a rosszakat.
Összesen 200 termékből **kiválasztok** 15-öt.(**kombináció**), ez az összes eset.
$$
\binom{200}{15}-
$$
De azok az esetek nem jók amikor nincs selejtes, vagy csak 1db selejtes van.
nincs selejtes, tehát a 10 selejtesből nem **választok ki** eggyet se, és a 190 jó közül **választok** 15 öt.
$$\binom{10}{0}\binom{190}{15}$$
Egyébként meg $$\binom{10}{0}=1$$, szóval ez igazából csak:
$$
\binom{190}{15}
$$
1 db selejtes van tehát 10 selejtesből **kiválasztok** 1et, és a 190 jóból pedig a maradék 14et.
$$
\binom{10}{1}\binom{190}{14}
$$
Végül a össze adjuk a rossz eseteket és az összegöket kivonjuk az összesből.
$$
\binom{200}{15}-\left [ \binom{190}{15} + \binom{10}{1}\binom{190}{14} \right ]
$$

* (c) Hány 4-gyel osztható  ̈ötjegyű szám képezhető a 0,1,2,3,4 számokból,  ha minden számjegycsak egyszer használható?
Akkor osztható 4-el egy szám ha az utolsó két számjegyéből képzet szám osztható néggyel. esetünkben ez a $04,12,20,24,32,40$ végződéseket jelenti.
A szám nem kezdőthet nullával, ezért ketté osztjuk a végződéseket olyanokra amiben van nulla tehát már nem lehet számunk elején, és olyanokra amiben nincs tehát vigyáznunk kell, hogy a 0 ne kerüljön előre.
Van benne 0: $04,20,40$
Nincs benne: $12,24,32$
Ha van benne nulla akkor 3 féle végződése lehet ( $04, 20,40$ ), és ezzel meg is van az utolső két számjegy. Ezen kívül még a maradék három számjegy **mindegyékét** sorba akarjuk rakni (és ezek **különböznek**)(**permutáció**) ezt $3!$ féle képpen tehetem meg.Ehez még **ki kell választanom** (**kombináció**) 1-et a 3 féle végződésemből:$\binom{3}{1}=3$. tehát ezeket.
$$
3!\cdot 3
$$
féle képpen tudom sorba rakni.
Azoknál a végeknél amiben nincs nulla viszont, a maradák háromszából az első helyre nem kerülhet a 0. Szóval kiválasztom az első számjegyet, 3 számjegyem maradt de a 0 nem játszik szóval a maradék kettőből **választok** eggyet $\binom{2}{1}=2$ és a másik kettőt pedig $2!$ féle képpen tudom sorba tenni a 2. és 3. helyre. Majd a 3 féle végződésből **kiválasztok** eggyett (**kombináció**)$\binom{3}{1}=3$ .Az egész eggyütt. 
$$
2\cdot 2! \cdot 3
$$
A két esetet pedig össze adom.
$$
3!\cdot 3 + 2 \cdot 2! \cdot 3
$$

### Szita elv
A logikai szita képlete 3 halmazra.
$$
|A\cup B \cup C|=|A|+|B|+|C| - |A \cap B| - |A \cap C| - | B \cap C | + |A \cap B \cap C |   
$$
### példa
* Egy 10-tagú társaság moziba megy.  Hányféleképpen ülhetnek le egy sorban úgy, hogy A  ́és B valamint C  ́és D ne kerüljön egymás mellé?
Egy 10 tagu társaság **minden tagját rakunk sorba** (**permutáció**). Ezt $ 10! $ féle képpen tehetjük meg.
Az nem jó ha csak egy páros ül egymás mellett. Egy páros egymás mellet ülését úgy számoljuk ki, hogy össze ragasztjuk őket, mostmár egynek számítanak, így lett 9 emberem (amiből az eggyik ember igazából egy pár) őket $9!$ féle képpen lehet sorba rendezni és a páros pedig $2!$ féle képpen tud leülni. Tehát
$$
9!\cdot 2!
$$
Ezek nekünk nem jó mert ez azoknak az eseteknek a száma amikor csak egy páros ült egymás mellé ezért ezt kivonjuk az összesből.
$$
10! - 9!\cdot 2!
$$
Az előző esetbe viszont nem foglalkoztunk a maradék 8 emberrel, lehet, hogy pont jól ült le a másik páros is. Ahoz hogy ezt megnézzük a másik párost is össze ragasztjuk, így lesz 8 "emberünk" akik $8!$ féle képpen ülhetnek le. Ezen belül 2 "ember" igazából egy-egy pár akik egyenként $2!$ féle képpen tudnak leülni. Tehát a képlet:
$$
8!\cdot 2! \cdot 2!
$$
ezt hozzá adjuk az eddigiekhez:
$$
10!-9!\cdot 2! + 8!\cdot 2! \cdot 2!
$$

## Binomiális tétel
$$
(a+b)^n=\sum_{k=0}^n \binom{n}{k}a^{n-k}b^{k}
$$
### példa:
Határozzuk meg az 
$$
\left ( x^4 + \frac{1}{2x^7} \right )^{24}
$$ 
kifejezésben az **$x^{41}$** és **$x^{47}$** tagok eggyütt hatóját. 

1. felírjuk a **tételt**
  $$
  \sum_{k=0}^{24}\binom{24}{k}(x^4)^{24-k}\left (\frac{1}{2x^7} \right )^{k}
  $$
2. **rendezünk**, hogy x legyen valahány k-adikon
$$
  \sum_{k=0}^{24}\binom{24}{k}(x)^{96-4k}\left (\frac{1}{2}\cdot\frac{1}{x^7} \right )^{k}\\ = \sum_{k=0}^{24}\binom{24}{k}(x)^{96-4k}\left (\frac{1}{2}\cdot x^{-7} \right )^{k} \\ =\sum_{k=0}^{24}\binom{24}{k}(x)^{96-4k}\left (\frac{1}{2}\right )^k \left ( x^{-7} \right )^{k}\\ =\sum_{k=0}^{24}\left (\frac{1}{2}\right )^k\binom{24}{k}x^{96-4k}\cdot x^{-7k} \\ =\sum_{k=0}^{24}\left (\frac{1}{2}\right )^k\binom{24}{k}x^{96-11k}
 $$
3. **megoldjuk** az egyenleteteket
**A megoldásnak **$$\mathbb{Z}$$** belinek kell lennie**
$$
x^{96-11k}=x^{41} \\
96-11k = 41  \\
55=11k \\
5 = k
$$ 
$$
x^{96-11k}=x^{47} \\
96-11k = 47 \\
49= 11k \\
\frac{49}{11}=k \\
k \notin \mathbb{Z}
$$
4. Felírjuk a **megoldást**

tudjuk,hogy 
$$ 
k = 5
$$
Ezért 
$$ x^{41} $$
 eggyütthatója 
$$ 
\left (\frac{1}{2^5}\right )\binom{24}{5}
$$

## Gráfok
### (Irányítatlan) Gráf

$
 G =(V,E,\varphi)
$ rendezett hármas
$V$ a csúcsok halmaza  
$E$ az élek halmaza  
$
\varphi$ illeszkedési fv. $E \to \{\text{V-beli rendezett párok}\}
$

definíciók for dummies like me:
* Ha két el illeszkedik egy közös csúcsra, vagy ha két csúcs illeszkedik egy közös élre akkor **szomszédosak**
* Egy Gráfban ha el tudunk jutni $v$ csúcsból $w$ csúcsba n lépésből, Azt n hosszú **sétának** nevezzük.
* A séta **zárt** ha $v=w$ egyébként **nyílt**.
* Ha a séta minden éle különbözik akkor egy **vonal**.
* Ha a séta minden csúcsa különbözik akkor **út**
* A legalább 1 hosszú zárt séta **kör** ha ugyan oda érek vissza ahonnan indultam és egy él se szerepel kétszer.
* Két él **párhuzamos** ha végpontjaik ugyan azok.
* Ha egy él mindkét vége ugyan az a csúcs akkor **hurokél**
* Ha egy gráfban nincs **párhuzamos** és **hurokél** akkor **Egyszerű**
* Egy gráf véges ha $V$ és $E$ halmazok végesek.
* Egy gráfban ha $v$ és $w$ között van **séta** akkor van **út** is.
(ha a **séta** nem **űt** akkor van ismétlődő csúcs.)
* Ha egy gráf bármely két **csúcsa** között van **út** akkor **összefüggő**.
* A gráf egy csúcsának **fokszáma** a rá illeszkedő élek száma. jelölése $d(v)$
* Két gráf **izomorf** ha ugyan azok a csúcsok ugyan azokkal az élekkel vannak összekapcsolva.
* Egy **kört** a gráf **Hamilton-körének** nevezünk ha a gráf minden csúcsát tartalmazza.
* Egy **útat** a gráf **Hamilton-útjának** nevezünk ha a gráf minden csúcst tartalmazza.
* Egy **vonal** a gráf **Euler-vonala** ha a gráf minden élét tartalmazza, lehet zárt vagy nyílt( a ceruza felemelése nélkül le lehet rajzolni a gráfot).
* **Zárt Euler-vonal** ha a gráf minden csúcsának fokszáma **páros**.
* **Nyílt Euler-vonal** ha a gráfnak **két paratlan** és **n-2 páros** fokszámú csúcsa van.
* Ha egy n csúcsú egyszerű gráf minden csúcsának fokszáma kissebb-egyenlő mint $\frac{n}{2}$ akkor **kör**
* Egy gráf **fa** ha bármely két csúcsát pontosan egy **út** köti össze.
* **Erdő** egy olyan gráf aminek minden komponense **fa**.
### példák
* Létezik-e  10  csúcsú  erdő  a  következő  fokszámsorozattal: $1,1,1,2,3,3,4,4,5,6$?    Bizonyítsuk ́állításunkat. Itt egy ocsmány házi készítési fa. A középső csúcsának 6 éle van és ezért kell nekünk 6db csúcs aminek csak 1 éle van.
![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQAAAAEACAMAAABrrFhUAAAAAXNSR0IB2cksfwAAAAlwSFlzAAALEwAACxMBAJqcGAAAALdQTFRFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAWuWfQwAAAD10Uk5TABA/MG+vv/9Pj9+f739f0LCAYEDPcCDg8KDAkFDr8er8scPb4f7tV37i12r3+fKesosurPaH9fvKyMTj814DHTUAAAywSURBVHic7V1rg6u2ETWL195dP8HGz715tEmatM1N+m7T/v/f1YskQIAQGiEdy17Oh5usMbZmOKPX6IwnkxEjboroKbp1E26EePo8m805ZrPnaXzrBkHx8vo2b+Lt9eXWzQIhWixb1nMsXz9APESrLvOZC1aP7oJ1Zf5mmyTpbrdLk2S7qVywvnUTfSLeF3Zus0PtyiHbFpf2j9sf7sTjP6bKy+lRkGAHbhcKKbfvpDafvePE39L9jnvGgpuf6Lq5KOEuWMBahcMzZ39fgMc8Dp4hbUKCP/9N/ygXbR6SAzz+z9Irh5fVjHWKSfO95wfsB3bzmqmxsH2udMAkYa8/0lgQL+vPP5Fnf20HcA4sD+0L9wo2/9lUf/c5YML6gT2sfb7BOoCj1P9xB1yu1y4HRMdH6gaiPABO8viXXK5p/veuywGTOJ8PLB9kZZR0mtntAM1NdwdOAOXD1DggehwKnLvDWeOASZZfu3psFww5AY7qSzoHTPJ+cOmtVTi8a/pzrQPY2PHurV0wsKGu45rWAZMHiYF8/3fbcU3vgHyP6M1Tq3CIdTMavQNYN2g+Hw40x6K1Qu+AQ3416/+KsHMs2/oqoA69A9iKoCt6Ctwmx0Jg20VnRI8D8nsv2nbAcyx0tl10JvY4INE7AJ5jsWKb1kQDB3SNoPAciy3b8nd0Lmt7HJBpHADOsVizLcqvd25u9TiAXVZ/NDjHYs+23WAHKG/G5liGsO3ghQHYHMswtvnoA7A5loFs05poNwpgcyxD2eZ+HoDNsQxmm/OZIDbHMpxteS/ZucHf44C9wnnYHIsDtrleDUJzLC7Y5ng/AJpjccO2o6YT6N8RauymYnMsbtim3BOcK9DkDptFN/YEoTkWR2xT7gqbOECxKwzNsThjW/5ac2/TxAH52vtUfwmaY3HGtqui1QYOULUamWNxxzY7Bqq+H5pjccg2q06ok0sd73eeY3HINk4B2uYMG4KPDdogcyxO2cZee6MEQfSm+H5cjmXimm3k6WhzCsIAyLFUcMs2NjwS1uVsDXZq2uo7xyLDNdv4qmJl+O0r9u5Wp+E1x9KAc7bxdaUZB/gavO1+jzmWFtyzja+W9wY7C3wL9ty+ov3aQTmWNjywje8fv/XuLfHsk+rLdWE5JMeigg+2cQ7oU3ZFAkbx/L3lWJTwwjaxv6xJ2U3FhovyOXvKsajhh207sX/+pnRBtBa51xMyx6KGL7bFZTLtedrIMU2fi0ubrtEH2Af4Y1uRRMlHhNlqtX56epquVrMy+aZLwABHAY9sK5IoamgTMMh5gE+2RdcuF5yu2ihFzgQ9s+39emxZf7z27Vi4zrHo4J9tcfaptP1yaebdlUCuBhFsu5T2GzYKuR+AYBvZAW5zLHog2MaYQuqcnOZY9ECwje4AlzmWPgDYRneAyxxLHwBss3CAwxxLHwBss3CAuxxLP4aw7auZSRLAwgHuciz9GMK2r+dLgwMJNg5wlmMx/Sortn2jCp82LByQLhzlWExgzzaO3q/Mbd/vKQ74Yv3CTY7FBNZsM/UAfSqcf/q3TnIsRrBlW4mp/t1UBwjv/o79OzDHYgY7tuXYGpxRjX9fucpEfFKy67v+zy7RlWMxg1VG73v2lVmPB+jiEym6fmD/DsqxGMImo/cHvtWRnbs9YCM+YcPL/HSWODAkx2IKm4yeODSbdnnASnzCn+UpZbduoqE5FnPYZPSEB1b83lnjXivxibD/ndMgHpxjIcAmo1ccnFZ4z058Iuzf7Qv7h+ZYKLBhW+GBa9MDduITwa50L5s0LMdCgQ3bRJDyiWvlAUvxCf+wrGb/ZFiOhQQrttU9wB+EpfhEtr+29hqQY6HBim2iA8g4fXLnWIpPhA5h22bGgBwLFTZsa3rAUnwinn+n+Mcux0KGDdtEF/jO7lz+yP5qD8rxF0+uVtMn+TXJWr68PGvFT3G2vRQLC8MciwUs2CY6gF11Y3N1HUtzwv20+phSfJLK9vfstUc73xUkyGwTHigDqPH8o1X9s6QOTohPhP3XDvbcAES2cQOWwgM/1S9G+3mnA/hKlM/5z23p1s1hzDbhgT+y//yp/hnc/tM5ybLkumk4ICrJtgnQfgKkDuDH+hWxvCn+jNJNbf4Xixs32V3bXxnSXJWyfk43KIrtRV6ww2ZPLxQID/y5bitLoWr3qiNp1DGYPQSM+OfchsYYfm72em1kD2L/ZPJ53hZAsmGk577jg9ivSjOyp9s3XxWLR3pSJzCoEs35XKI/X83HybAqY1lAJT6RI6D7AecrwE93b7/qsAl7Lck3E9hsYPk8VXqBLnUKEZ0i+N3kvVoKLVWb73SpU4hQiU/YJGe3mMt4VpCAKnUKEqojhzzzzPv47UXMeGbqeykHXIOE6tBpmUXnOykHvsBsn+KhHnEOEnOFA0QK4FROBblHWv0dVeoUJBSDQMEA6WXGgdaSjyp1ChFK8YkIf+kVlodtyZupUqcQodSPJE0CCAooBad3/ksTSvGJouJOonrfIzBA2Qew9VHdskzVWT5CH6AcBdhUuG6Z8gAydRQIsrayUnzSdsB7lwOM5gEh11ZWik/YrnetlYkqVMxmgoH/fqVSfJK1ujzlHpmB+OQ2v19JiTXlmvbQmve8KcK9fzUIr61sEWtq8Ulz2Gduaj7s3v0AcG1ly1hTik9Yp18VoOSn4JoR0CM+wdZWto+1XxTkFhQo9gD4Gatmd8e3jjs/HFpb2b6y9JpHTKuRB35sYPVyiNev7C2tE81iV3ihtgBaW9k21l7KEzTtMk5x47xBe/e3DLj9uu1gZG1ly1h7WegPz1dHj5hjW/dn0tXlokEPZG1lq1iLmx2mSnySlDlwxWdHjaCbySRD1la2iLXDVLL+xM93dFRg/BJI5yRTPchi26zk2JcOo6ABsrYyOdaiqXTy43TOij9txSeLVDqg9cwGXWRtZWKsReuy28sfXib/aS0+2a+zS/Up0whZW5kaaz9XLdmk6/rGv534hLNnsTtXNGCCCUxtZUqs/So3Y5Ol8hiw+UueIidLnf6aVcfSZtNUHjV+gtRWpsSa9BiO11QeA47Xg305uXepD3wVIowcnxdVVHqrrUyKtVi8/v3f0novKBpqXU7uIPeBa4kTrTyK89rKtMrSBUH/LlsvnYEYUE4ulfrAf/yz+N8WpV3XViZWlpYnbgzbtP6FQ8rJHc6tg7rt8xWOaytTK0sfas3bZG13D5I6RVIf+C+1pY5rK5MrS1cNPHYcoR1aTq7qA+eqjRK3tZXplaXTommfu5fwA6VOUfZVGWDG7RKgxgC9snS+f3H6t/57BkudyuFV8TDd1la2qCy93b73x9owqRMbbLcnddOc1lLyV8d8kNRJWJFeVF/vtJqWzzrmA6ROuNrKnuuY20qdYBX1AJWkZPFJjv+YtAtWUxFXue63wgFGbdN+s8uqmrhYoxXS0BHTaV1VXPVKkgNwtZVx9UtJ1YRwtZVhsUZzAK62MizWiPWkUO0C1jGnOwDBTGAdc5oDUH0TsI453QGQ0SnUPgBWWznQUQA3Qw10HoCrrYydCRLKKqJqK+PqmBMdgKqtjFsNEh2Aqq2Mq2NOrSyKqq0Mq2NOdQCqtjKsjjnVAajayrA65uTiuqjayqhYIzsAVVsZFWv08sqg2sqoWLOoLw2qrQyJtejpN7oD3Px+ZX/bvMaaLDyYLz+RHODm9yv74S/WVMKD+X8JIh8Xv19pAD+x5kTk4+D3Kw3gI9ZciXyG/36lCdzHWlt4kNPs0/8qF5iKfDC1lR3Hmkp4UA6DBUxFPpjayk5jTSk8qOYBVJEPprayw1hTCw/kiRBR5IOprews1jqEB7WZIFXkA6mt7CjWuoQHjakwUeQDqa3sJNY6hQfNtQBV5IOorewg1jTCg3nzt2bIIh9AbeWhsaYTHhSo1gIWIh/vtZWHxZpWeNB2gL3Ix2dt5SGxphUeKBwwQOTjFbaxphceqBxgLfLxDptYowoP2PfYiXxgIMUaWXigv+nuQBcesLsCpwABdOEBg83PZIYJC+EBA13kEyb8CQ/uBD6FB3cBz8KD4IH8wfQggTtqEihwwoNAgTtuFihwBw4DhdZEp0dOA4V2Wev00HGYAAoPwgRQeBAmgMKDQPHR+4BxFPjw84APPxPECQ8CxYdfDX74/QCc8CBUwIQHoQImPAgWKOFBsEAJD4IFSngQLlAin2CBEvmEC5DIJ2CARD7hAiTyCRggkU/AAIl8AgZG5BMyMCKfkIER+YQMjMgnZGBEPiEDI/IJGhCRT9CAiHzCBkLkEzoAIp/g4V3kcyfwKfIZMWLEiBEjRowYMWLEiBEjRowYMWLEiBFN/B/PNq1cjG9IdwAAAABJRU5ErkJggg==)
A feladatban viszont csak 3db 1 élű csúcsot kaptunk. Tehát nem létezik ilyen gráf.

* Igazoljuk, hogy egy összefüggő, véges gráfban bármely két leghosszabb útnak van közős pontja.
¯\\\_(ツ)_/¯

* Le lehet-e rajzolni a következő gráfot az  ́írószer felemelése nélkül úgy, hogy minden élet pontosan egyszer húzunk be?
 ![](data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAL4AAACvCAYAAABD/mSjAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEnQAABJ0Ad5mH3gAADBQSURBVHhe7d0JuHVT/Qfw3TxQkgaFIpQGEs2TlLGBUJEGKlOpKCLV00SpPJEoUyK8ZUiDpyjRTIU0kJChSUipaJ7O/3xW9/tazv++3nvve86559y7v8+znr332mvvvdZvfddv/da4b9fpohkC/vWvfzU+dcc73rH5+9//3tzlLndp/vOf/xS/O9/5zuXo+va3v33z3//+t4QR1r2bb765+etf/9oss8wy5V13uMMdyr3b3e52zT//+c9ynXPP3u1udyvX4MjP8/y9P0l2zzcd+TuKZ57lx/373/8uzyRMXOLI33OJl3eCa98FfsJzyy67bAknXt5DFn/729+aP/zhD83SSy/d3POe9yxpESZyEdb77nSnO5X3ec69pF8cg3/84x/lHZ5zH+5+97uXa+G81zPiLO7O8x3vde0Ijr4N7nvGUb5w7iVu3iW94pg4CeMdnsl78s76W67FgQP+vpVngjr9ILyweb84cOTJT3jyDYQp3+4+OBTii0QiGUgYv0RMZEU6heJPf/pTs9RSS5VrhPnjH//Y3Oc+9ykZ6z2EGsJxMjlEyHccZXQQEvOP8EIO4Zz7XgQujMxEnLzT81y+LUyEDMK7j8SBwsvvHve4RzkGeVcyJKT0PukUF37uhdzOySnp8v2kBfLOm266qbxLPLzPc67z/qQVnEsP5QB5J5fv1d8IpNW7UhBci5ew4u/bzpNmR9/xPeF9P/f4JX6+iQ93vetdi79vkF3CeWfSDzXRPQO+D96XuDuWtHQ/OhTi39ZnEqmEiaA4573X00X9TL4xE+Q9U31H73d741+TEdzjEAOBFHr3ZNTivtn7rRxr/4Df4t43GSZ7l/cs6jvBZN/LM9OJS76xqPD1O3M9GUqY7s3pS6DFtEHMSO4YkcuAkJrGoqFoq2hBpP/zn/9ctFg0cYv+oCX+AEG0veJF/rhoeibJVVdd1fzud78rpsGKK67YLL/88qUA1GZdi/6hJf4AEJHWxE8V7Brp2evs/t/+9rfN1Vdf3VxwwQXNb37zm9IofehDH9o8/OEPb1ZYYYXmAQ94QKvtB4CW+AMAYkMt2tqsAZr8u9/9bnP22Wc3F154YfPTn/60aH52/f3vf/9mpZVWap785Cc3W221VfPgBz+4PN+if7j9xLHFEKFRq/F63nnnNaecckrzta99rfnVr35VujPVAArBV7/61eaMM84otYDwLfqLlvgDQMyaaGk1QDQ9EjNx2PO//vWvm2uvvXZhN67wwunV+ctf/tJcc801zS9/+cvSrduiv2iJP0DU5AekVgiQ2pgEQqevOVAw0s154403FvK3xO8/WuIPAAjOIX00eQpANDotbyCmbg84T+2QAsLu7y0cLZYcLfGHAN2RIb6CkEGpujbQXckvI4/p6tSlmRHhFv1DS/whgiaHe93rXqXL8iEPecjC+Ucau4hP05ueAQ984AObddZZp/TwtOgvWuIPAbWp40iDm/ujm/IRj3hE6as37yS9N/ryV1tttXLPhLZo/xb9Q9uPPwDEbqfBa8R2TwP2Jz/5SXP++ec3l19+efOLX/yi+dnPflYKxOqrr15qBAVjvfXWKxq/JX9/0RJ/ALgtkaZQJAyzRt/99ddf33z6058ups/Tnva05mEPe1g5VxNwLfH7i9bUGQKQPESP2YPI/JD6QQ96UPOEJzyh2Py0O9se6TVsFYy2V6f/aIk/AMSeh1r7Ow/xEya9PPFTELQBHN3LeoQW/UVL/AEhxM4RQu74ITxym7CWBTLuhfTOc2zRX7TEHyBC8trVCKnZ/YjvXGFIDaABzMzJyqsW/UNL/CGgJj2tXjv+CgDyM3Fi+7fEHyxa4s8SkBtq4iO6aQpZn6sgaOS28/H7j5b4s4TUAI4hPxe7n0uYFv1HS/whI0SPC5yH8C3xB4+W+C3mJVrit5iXaInfYl6iJX6LeYmW+C3mJVrit5iXaInfYl6iJf4cwsUXX9xss802zY9+9KMJnxaLQkv8OYQ111yzOemkk5rDDz98wqfFotASf47gyiuvnDhrms0333zirMWi0BJ/juBtb3vbxNktfw1psWi0xJ8DsNcmEyew6WyL20ZL/DmAD3zgAwunOZvcZoeGFreNlvhjDlsR2p3Bv8FWXXXVsj63/YnE4tESf8zxhS98ofwu6EUvelFr208DLfHHHJ/85CfLcdttty3HFlNDS/wxhq3GP/e5z5Ud1570pCdN+LaYClrijzH23nvvcnzxi19cGrUtpo6hSSuLqfU+ONo5gJ/rHIG96ocIdhD7/e9/X36RQ7O53xt2VCGOXA3X0t0bf9d2UvDnE3vhc34EkfTeVlqf//znN6961auaPffcc8JnfBAZcf4XIN033HBD+VOM/Jfn7uGB/wgAWeQfwEu6u9zQ9s4M6S2mliDEp6Vcg/WlEnzppZcWsrtnT0kJX3nllctfAO9973sXp9dilNejSiskbRA/8ebIov4l0Pe+973mBz/4QdlRYe211y4/frOduJ2UbSQ7lfSSkc1nEWPUgQNkkjy3YS6lp1fKbtF6qdZdd92Ffz/XcM82KwqCTbf4z5QHQyO+hPpUMl6JdcyuYUryt7/97dJL8a1vfav89AzxEWGttdYqG6k+8YlPbJ75zGcu/E38qGIy4veKWXpNKpPW73znO825555bNo5FcgX9UY96VMn4LbbYotjwU8GoE7+WgXNEl+6PfvSj5Wd3tlZR2O0lKs933HHHIgd+SC9dfusf5ekdtYyng6GZOiFqvTkSgrhGAj84Dgkuu+yy5rrrriv3COeSSy5pvv71r5cttYUddSjIi7O5pePHP/7xrQo6TcbMUSC++MUvNuecc075E+JcAaIiLCDxRRdd1HzpS18qeZvCKv3y+8wzz2y++c1vFl7ggOfqvK/fNRMMhfgIntKeo5IackgYASj19opHAFrdhqlqBPf5f+Mb32h++MMfLrT/RtVJI5eCzfFX+B1lMjNHATeFmLmjKle7ZfBJmq+44orm+9//fjH96vcvynk/2XGT3Z9tRx7Iy0mbPPerU/KQ18stt1z5DRJ50f4sAJzwA2zyYQJBOLQktf5QTB0JFUlOomQuQfi0I2335je/uWj7aHoCCHGEUUhsn/32t7+9mDxs/VFFMiSZDeKvsLtGeprc9OFPfepTJY0yNjLKc+x78+uf85znlP3yF4dXv/rVzWmnnVbIMpXws4Hku59bn3DCCYX4Cqp0kwETWNqZfEwc26dvt912zSabbFLkF60vfK08p4uhET8RlMm0uM/yc61E77bbbuUPIXpyNGhDgEAi73e/+zV77bVXafwRyqhC3EH8pQ+klZOpejA06hYsWFBMnYQB6QTyoQFf8IIXNBtssEHZO39x+PnPf17IpJeH4hg1kEvyFfGl31EjlYYHMsIPypEs/AOMrb/lllsWv9Rm7qV2nAmGYuooyclQkNEIEPLLJPNMkFnYusUeEnmeKcBfgoUZdSct6ZGonfjHrEn6IDIiG/6e1dA123Ky9/c6/8165zvfWczEye7PtpPX0iW/k5fSXOte8uFH+enZUctr0ArD5V01n2aCof4KKNWcjIUkmgZkzx122GHF3GHfIoXEJ3qEROsxidZff/2xmXpbi1emk4H0M3cOPPDAYu5Ir8x03z1O+h/96EeXefZPfepTC5nHHdIebX3BBReUyXUa8ex9flzSz9TZbLPNmqc85SnlP2BrrLFGkQ8XKEgzxR26GuKdE+cDg8QydxxT8lP6k1BEVhMggcarqk94jZ773ve+pUZ4/OMf32y11VbF9kWMvGfUXDIomRkXTcWBtg3TDjTwpF8YJg7t/djHPrZ51rOe1aywwgrlmcm+NU4uMogykE7++vEhclMj0PTPfe5zi6mjezMFX/g873ymGArxRZQLGZLAOgFa7Ko/DtQCCsuKK65YEv+MZzyjefrTn9485jGPWRhm1CGNNepr56p17RYFWx++Qi/TabdNN920EP+Rj3zkwt6MuYDkuxqcaSu9Cj1ZyHP8ULOr5dTs1hboyODvuZr4uZ4JhmrqLA4EcO2115YuPFWg0TyDMoTwuMc9rhSCDGDMBaSG0yg94ogjSr89Qshwsy1luEKBHL2FaBwRUwdZpYcpq8BbQaabWvemewo888bgFUtA+j1HXmkXsRL4KTAzwUgRX1QkRr+26s/cDSaOkUs/Oq4xF4gQ0evC05+va48W1F1rxwQkIA9pnalmGyUgrl4ZZJWepE/6mbembSjsGalefvnlC9EhxKcYyCOymykPRkqaEgfsOXYdAUg8O19CJTy28FyANHGI4C/mtDsN54gg4DhX0qumpr3Z+ZD8jtlDuTHrHNn40e7gmOf6gZEifjRbEptM51Rt/JFkrpg6dabqxaD5VP/aNtKeQtGbXtrxqKOOmrgaH9T5m0Ifv9QCyWvXCQfuRQ4KTNxMMVLElziIUAihFlYtoLkAaaozV4YjvGO0m2Mv8WnEnXbaqXnhC1844TMekC6F2rEX0sglb5E6hSAFJHCv12+6GDkGSaiESZRM14OjeiQUfkn0XIG0xKnak/lqAOhNr0lbwR577DFxNh6Qlpr4IW7SJ7/JwFHYcAEio8jDs0uiAEeO+Eq5Kj/HJBwk2K4C7s0lSFfI4Fjb90yfOr1f+cpXytGWIhrB44Qosphv0i2NMfHkbdLrfgoBhPQcwi9prT8w4kuQ0TmJmg4kOIJBgiSYMJRygnNvrkCakmaoC7qM1/Cr06uLFzIJTd+3eSwHHHBAuR5lSGuvQ2xp5HRVa9ym+zYOB1L4yQLp+S8JBkJ8XZH6YvW9W0gxHUgkEIhESmBKuvMkfC4g2j3KQeZGowXJ/ODLX/5yuW8oHzzz2c9+tjnmmGPK9Sgj+Zg8li5p4eQreei1o/mj9IKEjTykm9k0U/SVQSJ65JFHFtJbZAAGpKYKzydhUCfUufu1IxykicD0dhgQsZTPHHZTf3tNhVr4dSb0E4lfjXy3hjDiZhBLnGlvCzGkQ5qgfk5aLFIxO9VwP+jqBWm1+ipmUj8wWZwh8U46xbUmqmOejV/Qew38OO8U/zq/IPkfPgSTvWuq6Cvxjb7tvPPOJeIz2apaQmh6ri7N8UtCI1TCRgbkl+lWaBkEOv3008t0XyPApjwbCPM+z3smGZLMynv7hcQv8P58M8j3kd5Cc6OWdjxWW5KjAmwiW+KdZ+DCCy8sS/M44xxgHYOZnP20+xNHx7jIPfFyLb9dJ91Ja8K67zwEDkLmwH1mjoa9/M59tQFXw32m4EzR15FbrzrjjDPKBKvVV1+9RJp2soh6KohwVHVqClrbsLXpqd7NuU+7ezchEaywxx9/fDEDEJ2ghTXPhROHjTbaqMyLiaC9RxjPTybYJUE0lsyBkMc38x1+SG5WqqWHnBFrdm6maWStsUEtz5KP+UpmsLrOe2ro4jz55JMnrpYM4hz0kjgNz8haumKy5Z7wajHhkbmeY5X4Q75jbr5w5uqQwyDRV40vMc9+9rPLxCILxWEQK6VCHtW7datWHHGG/WlKpgIS0YzWc2pk85dJ0SLiytWZ2094r4znZDzke/xitoi3eIqfAk/L67LkbyG2tIQk4q2ghICcWg522GGH8u5+kT7wLfGSBt/XCM3oq3jJA8tCkVYtq7Z1bv6RGbbCG4l3HCX0lfg1rKYCGrffyKiewmUdrgltJjoxG8A9GcZeZgJZvKwmUFPQSmkXCKcQhVj9Qm/hqq/Bt9myCKNQMlNiOjiaqmw9rrkrCjZTbVFASki//6AQWYljtL4eprPPPruspLJTgtFkTjuPn1pMnki7dCWuo4CBTVI7+OCDm913373Y+uz+qYAwCXhxpg6HOBrQH/nIR4p2FB6h0i1IoyazzPl52cteVrr9aB/v4a9Klimei1buB3wXEufAt1yLm0L54Q9/uKy5FRfpEYcUROdGaC27U4uussoqE2+5NWhc6bLc0NrcfkI8xJeiIFcO6V0zReyEwLQ977zzSuNcGqRRIdTeMJXchldMX+kh52WWWWbhu4PIaJimzsCI/9KXvrSUetX1VPd1nCrxA7XK+9///qLxYxIQvMxxznlGD4j9eLbeeusyGYr2kUkhv4zk1y94r3hIT7QjP855Fpt//vOfL1togO+LrzCAJGxii801WE1cGzbERZxobaYNpeFamrSXTjzxxEJ8mp+fghH5I7C2lQJpcyzX8iI1k/cEoeDYEx+pCIbgVOPRwovDVIkfbW7J2oc+9KHSI8JGdt83fV8mySyk1s6wv6Q57lZv+Y5veA/i6xLspw0awoT4iReH0IhP46sVLQ4XLqQJcaTBDMXXvva1zcYbb9ystNJKE28fHsSXDMkqcRQv8Zc+a5iYOvI4IM/UoDhA7nZJ0NlBxt4Jng9CwbFt3AZsOxlrzeRUST8dpNeARmQCaEeoQkM0QOosXzQaKBOEVyAUJAWACYRcTArk75fzHc6573G5J0N9Ty3kmJ4p8eYU1NRG4ix+4pvnh+nIWJykBSH5USK6UMlbgRDfGiksCrBazH3XKdijgoHERE8KbLjhhuXYbxAuhziqUcv0aBQZxB+5kAnhDO3rGjSoJtNkQAoN0nHx65eL5hYX7877QwrfFkcFlhlDmwsjzrQfUunSFOfsPlG/f5gu8Rd3aaJ0FAJHSsVReoSFaHvpEN49YWj4aPZ+QMfA0UcfXcytbE0yHQyE+LrcQONmkKDJTYtAfGtxaXEkoSmR3tpV/ffPe97zSrgsbk7GyCROpvRm+JK4kCRED/H51eSxN6ZaUV89LSqMgqgxKE3kp5EY7TtsxzQJcV3X5HWuJlJIpS+mi9qKTJNO+UEOnusn8clHFy5Tym4UGtfTwUAWm7Nd2X02QmKH9zpddboh7fr1iU98okywUnoRVCarXjWoDH4gMZIEBBwhOxI64duxQHjC9gyNaocCzkCQTArpPSczkiGu3esX6jguCikU4o70TENjDwqzQSp2sYKcNbezAbIhL7IR3/gBfx0K8tPIc2or6ZaH5C/uan21lmvpDSaTD/LKTzX54tpclIV89d7sPWrzraliII1b24BkYGWqsPTOlFuZvbjGLZfrQANXJhCaXRkITjiaPyYOpPdEJnheIaszdpjwbRpSIRd3DUWF3LZ50iCurhP3YaOWr/PITnxcG3izQF5eG4U2lkLuzE75qVfHv7nIn0Kq4T1BvjOTxq0Cx1TU4LY73VRlNRDim6Jw6qmnTlz9f0i0YXi7Jlhbq2qn6aKNp0t810hzzTXXlHeauUjQiAXOE77W+K5TLfdT408Hvp9GoFmWagC2PfmIk7jmOGxEvkBu5CkeIZdaSm+avDJSy8RVe8lPWl5eICVZS6P3pQDU6cl3Ztqrw1IwamywEp+mgoH1488EUyW+cIl2NDXie8bmS8YNZA7C5DmuJpBj/J3PFvGRiRNfU4vZxDQ+RZDMF7/Ee5gg5yCygshKnBRctRYzjfmqwS4PFGAk17uWGgJSaOr05N5MiW/qu3+BTYf4s5PbSwgZEpsyiCD5IVIKBydsClXIH//6HbMJ8TClgtmj4EsDv5ogswFx4MiNeRhFE5nyZ97ErqfhmTbCui9N0uK5kH4UMJbEJ0DCTmNJxsgI0BBk1wuTzBKWcy2sDJEZs01830YIcXNEHBrPNY2ZAjpbcSQvDhKPFMSYZ2RIljoWzCnSteg8c3SkqZb9IJAJkdNREmNJ/F4QvuqWpiRwLjZlL/ghPkSDzZZWTQHkxFk6QiREkQZpcuyFOHOT3RsGmDFcNLmCyiSldOq+e/cc5U/vYFc/wMQxLSYm1lQxlsSX2TWxkSS1ABdt737IJDzhhygyDMFUxbNFfN8VT0eFoE4Xv5DKcVHQpThoJH5xiRvnnHyROrIVZy6If2rofsEs1l122aXE49hjjy2ynCrmhMavC0AyJJkC7sfxE+64444r2klVfNBBB5Vww4Z4xNGQIRYolNKALO4vCoOcvFbLrXaB87pAaMgqBOIr7kH8+k18s3/1KvlZiJ6w6WAsiU+ANSEINlUpwoQ8yQBho4U4vQ82ZPIc6I+eLSiIXOxgDqGi+cUx6anBtND7MSz0KpGci5s4a1uJa2Sa8Elfv3HWWWeVCX6mdvg91HQxlsSvhQ8pCLWWmQwywNxxo7kyaP/991/o3wsNNAI1sDRoSAttKQ2JCzvZuTSlgE8G3YimNtfy6CfEIQokLnHkH9nzq+MvPikE/G8rDdOFBvSuu+5azq33mMnI9lgSvxeEXbvJkHsIZWT5Yx/7WPnxQO714g1veEOz7777Lpx3NAzU8c957TcZDN3bUttKrn6jNw7TcTUm81sSMHFMUnvFK15RpnfMBHOC+NOB+R2W9L3yla8sc0xAv3MNv6j5+Mc/XobeDbmPMmLj13Pi5zosPGIaHnLIIRM+/4NpC3aomErtN++IX8M8E8jmTGBVlBl/2gJGUvWrjyJkrq7OaFKzPPNbofkA7Tk9OpaUcpZdmpxolq6Oi8VhXhM/E+lMWQaT5MzwQ6bPfOYztyoQowZxNEUjSxctqjHNYT7AVG3QuI2zu4NxBL+NtUZ5cZi3xNeY1LtDiOZ3WL+L9HopmDqx/0cVe++9d1mEE9hape47n8uwaZitWWpnqru5OpaiTqWLd94S38CVLlCzCvfbb7+y6ENPDruR2TDq2GeffYqpZsRSr8l80fbAvpfu2tnLaTo9R/OW+LV21G1pBNTxNa95zYTveECXYd2N2GJqmLfEN2Jrawx9+fbmsfrnXe9618TdFnMd85b4sMkmm5Q/pdPyFn60mD+Y18SfC7DEz4qnFtNDS/wxh6480zBaTA8t8cccpmBwow6NcG0pO8NlxHw20RK/xcBhp2r7G73uda8ruyrbUma20RK/xUBhr39bpWR3PciszdlES/wWA4Xd60z0s2mYaSCjgpb4LQYKC2aOOOKI5o1vfONi10sMEy3xWwwNpgzDbG2JWKMlfouhwBYgfuJh/52Xv/zlE76zh5b4LYaCU045pUwMZPLY83+2MZDdkmcKiytMtso+M2ZLTrZbcmB2JU3iB2nmpZtabBWO6QfuCcuu9F6Lt83ey/td595UVuxMB3otuNqmdZ30gaO1o5w42/sxfzm040L2pqnXuA4bZGTBRyb0OZcOciRD8bJRq//z+qewad66Lv3mSLzNhzKBTj6+9a1vLVtCsvfNm58sTdPZLXlJMZZ7ZxK+THHfLsnWxZqLTaNYRmjvSUsMLTAx154Qk2Ge9x7fkimDgG9B3p/vQabO8jMl2g4P1svaN1KaxdUUBD+M8N8u6wU0EAcV19uCdHC+Ld7WMAAbXXooHfG3oMeP+MyLNz3aFGGyt17A3vV2UrZzstVRCgjUxCcLGOavgMZW49tdgKA++clPlr8eWnNKW8oMhFIIaA6LEgiRBvJeiBblQtI6I5YUeTdIU1wKHtKIi12l7ZBs4QtyJP40n2WE9v2x4zDi17XHsJD4pnaMXwqC+Kup/Fhbw9Vu1fJFGsRfrWsvTenTj29CIKUEk8l7mBp/LG18Qjfs7Q/fVtsjkWpYTcFsQHw/C2D+GEBBNMQTxnmIGT/P8++Xg7zftcLlHBwRiYljP3nzbBRa/omXQk+DIpR7ve8flovGz0ZRCB8FwokjOVv9Zctw+YLsftIgXQqF+9IBU1kSOCyMJfFlCltStYnosYU5GgOJmA0KBS2EaODoWQgBB40QHqHB91NA/SaTc69eQYVACoYldrY/l9bZAKWgFhJXskJqhZJW56/gOhdf5o8em7QHxJ/SYfdTUHDggQeWKeD+fUvzWw8xWxhLG1+GnHTSSc2hhx5aqlsI8YWhoVSVflJgGeELX/jCYi55n0xKQXHO5b2DQMTre76BPBp/TAEaEbmZZQhDiyYuiY8dBNj6TIZhg2xoeXFyLu7iCPLAwh0dC/yFk39cntPesvuBjV0VkBrywI4W1kRERsO08ceS+ISve8yfwe2RAzXxaSdEsi+OBeT+/E2QCJewIb339VsEeV/IC74F4iYeiOCv5vaIQaIgcfMOJPBXeDu/TWcn4H5B/MlRfMSRQnGO1Oz4d7zjHYX4Cq68A/FOGIqH0vGzb3kKkYmt/7RdIPIaJvF9dGTQtSM7XQF2utVk54orruh0tWHnhhtuKPf45z50tUhno4026nQ1e6cr5E5XoKTX6RKn0612O90M62y66aadBQsWdLra9VbP1+ddbdXpkrFc98t1C2ZxtV++61s33nhj58orr+x0Sd3par4SV3HnnCdNK6+8cufoo4/udG3mW71rWE5cycd5l9ydrh1f8qZbADpdM6az0047dVZZZZUSV3GXB13SL0zHqquu2tl3331LWr0rMuBqxO/cc8/tdBvJnZtuumnizuAwljZ+N95l8hPNoM+ebUnD0CaOenJoG3vN6CHoCrU8F20DOe9majnSUv1yqX18I98RZ+Avvulx0mvFJIimFx+1ApvZz6t1x9KMvd8YhiNL8RJ3piMtbHzBPXG2QzE5g3QK5xm1hB4pecNJD3NolDCW3ZnI4V9RMuKSSy4pPTzCEDowC1SxfgqmHxnJIKTyfoVBpnpGRoag/Ybv1IULfBc5EBpBmD6IIQ3CI71+b/PXbZ0hncLPBsRdQ9X3yY0jK3HU1ao3x59QmEHMIWlToPXjx8xEfjZ9LePJ5D3M7syRIn5A2GxCtiP7PsRHitoeJjykoHU0/mhIhLGRqgEgdqTnkVtYLtosGjb+g0K+me/UDhn05tCeaid2v8Lg53V+YCwNCj6SpdDMBsgqEI8oC0oEUcWfnOWBP8mLt32KHBUOYXrjL/29mLcjtyA6SK//miAIklAJDvEJm+AVDgXDkLmGFq3pnkKCLEwhbpSX5dGSajUak+Y0GCdtCJM/tYt/Cu6wIS9SC5N9zlMQ1Mp6pzj5IC3CyS+1bszQGikAdXpCwXnbqwMEp2o1CIL4fsuPyDSBqEb4NfghvWdBWGEIuTfsKEHhFWdHThct7W4bc/+JhdmMf+JHntBLFfcoKXKmoZ2nFkhbALwnSKGZbeLPXv25CBAmRzA0HUHVQkpmpBCA+8wGpIlZkHCjDPGURnEHBZ5DHqjTOBvw7V4ZJk6c+GujIKtzsneN9PKtDsfVJtNsY+SIT0A0BjIQoPNaYEgeR6jpX9Y4RJg0wEad9CC+Ibl0ckjEQQr4bKJWOiBOkXFv3ISVf/Uziwo72xg54tPUiKzaTNVZa2+CJcRc05gIk9ohjl9vpo0apCFmgPOkKYVa2vnNFmmiqRM38ajJza83b3IPHJMf8RsVjBzxCTcZTWAECfwifMIOYSLYoM6cPDuqSNzFOQWcdsx55DBbIEcu8hafyFW8Q2b5Is5q3dS8wrsvnPAJOyoYSVMndq8GE80dAhNwyA+OHD+CJvBRrFYXhdRUIVYKcNLg3mySxne5yD1+4sSF1JACmzxI+CB5NSoYKeITDALQGhp5usuYPREmoRO2QoE0IUqIU2fOqAl6MoifdDHpwKAcl8Yh4ictswlx4CByjQNxFWddl5xzcU8Y+SKd8mhUMFLdmbowTUyzlM0yQudrrrlm6d6yTtNqJKOCBCja0TzJmCTFuTDRSqMKBVuXrenT+vJPPfXUUqB1ZxqIsxJLX7ieEkSaDUTbR95kGyI7D8Fdk3XkzY9L3gQ5r/2Sb3OyHz/aGCS6Ji8ggZmK5m77rYt59JmyYCAH8Q1m+TFbPbclgo0gkxzf45f33xbUKJ7LOyE1iHeEdK65fLcXiUfSWr+rNy5qNGtsTUs27cIfuhV44aRX5ptuYchfutPlGYhvnXWJT+3XD3hv3plvRA5JUy2XyKaOn+vaTxi1Oj+yMbfftBOLVoxcW7po9NbIO0UAwnHyIt8E5zPBUIjvEyLqGGHREK6RDfFkvn/PWkZICLophZVQtr6hfRrwgx/8YBnUSpdfPzAV4k+Whl7w5zwLCeP9eQ+4b7TzyCOPbBYsWFCmKhh5Fj7fpPEQ3l9a/Mu1HgF1Xzy4gB/Ufv1A3hvk/b3HpFX43vjlWINMOAqOprde1xRneW3qg4Kv0FMC/MgMJ4wVyKfe704XQ5urU5MmghFp54brLU+zWsefxGl/99KwlWB+nrGAGSm8L12eS+q8P+MBrmmjEJ9Lb0X8FNpkXO3iX4d3TDsl19JiiN9CFMsjU/gdIyvh+JnsxcTrHfqHmmwhQuTbDzcZfCdy4aQJ5FX9TH3umcRPnKP0rLmg6PxT2LoKGwZY1aXGNx2FWUvhZYCMPBzjFhXHqWDWND4yJMMl9N3vfnfZbcC8myTQM4FrDT/L1kx86q36lwQyIqQWJza1qla1S9uItziB+EdkyXwQP8+mdhDOdf1srhGfhqft/WKUFkvB9j3hZLx3WKBtIYdaLiTzLS5x8X7OczEN+gXf4BYF3wfxAXEU3nXuBWTlvrQhvYJvIbolpJFjnkH45z//+c3GG29clJ25P5QB+UYhLgmGZuOHWMk0iZdRSMfM8ft8i5YRn/Z1LxkNrpHRgmW2Pxu5X/BucZJhCpTv0DbaEtG04hkSiBen8MqMaDDv8K4UCOe9z/IXd7WcbTloOM8lranKkz5rVLfddtsyPTkFNDIE16m5+PWT+L7HiY/3Ih2Xb4N0u6fAptYUJ2n1HCdM5EG+0ibtfmpB43uGA2G83/P+9mKm7RZbbFHI79u1HL2X30wwNOIjCIFEiK4l0OeVfnumW1itN4f96x4SRPiesSjD9nNIOdMET4ZkJIF6r6oV4X0/NUsEzdGuMb8UYJk2WY2Q97pOhjlHUs8yc9i33iEs2SSM7+nRUcOtt956pZHvGyFV3k02nufEyXP9hO+QCWKTBZd4Jq7gu9JEKUlfHU9h6sIgzhryFpvbl4df5Oc8aaOA1l133ZLn/jusJg58jxt54suUaI+YD4F72WOGBoiNJ6EEIqwpxqbrvv71ry+9HcyQfoEAE5+aVPy43HcPpEPBFUfaK8RPmLwj4ePHCeN5z+jFkmYk8A1wD9Q2/qyuF0svRwgjXN4PCBWNWRfAfsA3EB25KJ5ofXGp0yMO8pA89NCQCflEHo5Jt/h7p0btmWeeWcxb8N6k3TOuteVMz95uu+3KpgFp27kfRA7TxdCIL1FJmISLvGufl0iZRuPbGc2Pi/VtE6iEKfn6tbXy7bVel/x+oRYm1BklHhASBMJEC0eM0ViOgXfHJZx3Syfin3XWWaVnB2ncl16L7JF+gw02KPatd6ZwOPcux8TB+3xTHPsF6fLemuyQNEyG1Dyeg8jDs/y8k/2utjvssMNKfvOTp2qMyNo3bRZgUY7VdGo9aVOgvMt767yYLobWq1MLLUJMhkWTKNE0HW0ukYQh8euvv35ZRmhVj3t5vl8uGdyboREuJ47u1+E8mzCTacSg/laADBrrTBjpNNpp/xrXsWvtrmB1WRSFZzjtA9/yHUgcer+7pPAu7/XNXNdQELnIIwXTM2p1aWI2MhldO7oWd4OT2naOlB4/fJDWOOaNPLcdpBo/hamWxUwxFI1PA4ikCMe2l1Cf5ggK3MuKJA6EUyAkXKEgHELMM/3AokSQjE48gV8c1PeCpLX3Xp4B59JLy2nT2P1N1Y8cFnH7fU7aMvme9zIjFA4/UXvve9878baZV/mLQx1nqNN0W+kL+CVMzqVZO4CJd/rpp5f2HRngiTDaVxbi7LXXXqU71+ClPCcvfKjfOVMeDIX4yCqyXF2VBXWmuR+n1IMo5nmJ92w/Mzrv9p3JxBE/YXq/Gy1Xh1nU+/gFzqMtOWaOKQuIL7MNXvlW3i+8a4TJ4N3222/f+JMgUrhXv39Q6E1Tb/rioDcsuKbh5a2jQSvmnt3ismNbGrVqeQNZasRwIsR3Dqn1pouh2fgtFg3kRgJrbhHf9npMPEj21KSuz21Ktc0220xcjTa04XRb77bbbs3mm2++sMbThW2elsJP62sD0PjW7jIHFZJF0bSWxXTQEn8EMB3i0/hMPxpPH7/d5Gg9dr99OJlJo4qtt966Ofnkk8u5nju9VdLH0fSOanO2PJNOOlO7JxyQRa5nqvEHYxi2GBj8WCHVvH/GGgAzEMQ0YiL5V8CoApmDXXbZpRyRV8GXphCbhqfpY9YMAi3xxwiIYSIfGM4H3Z02ltU41OA1pXlUcfjhhzfvec97SuPVrNRobY5WT69Ufe6eQpH2UL8w54hPQIRqVDB/2ZsrMMJtZBuMadCIpjMb45DWQw45ZGGHwCjCeIS5RzHjIERn0tRkrwkf0rtX3+dmijlHfJlvW21zeoz6nXjiiRN3bgGB+uHwKaecMuEzHsgoJxjGZxfDPvvsU+byjAPIXmGNbR4yIz/UZk+t5RMOUhiWBHOO+Eb5zPS0LzvoG69BYDvuuGPpIz7ttNMmfMcDG264YXPwwQeXH6ntt99+pXELekLGAUj8vve9r9RcBuhCco1ZTi+PkV8NfSRXC8SloPQWiBmj+4I5CdtNS972228/4fO/bch32GGH4r/GGmt0rrvuuok7swvx6jb8OkcddVTn+OOP71x++eUTd27ZQnsyHHTQQSUtxx133ITPaOPQQw8t8V1uueU6l1122aTbqccv6a7T79gtHJ1uQS/H+M8E86ZxqxvQTEeNQ91oJsOZ/dhiOLCkdPfddy9dlOYmsfO7xC2avkv4iVC3NmP4c12eLtT0Ccs0iukzE8wL4hsd1NV37LHHlm4/f+CzpK3FcGBOjt2fkdxqK43x2OympujpMSfJzNvFAeGXlPQw54mP7Lr49Ia85CUvKUvcWtIPD+x17S0DbMhvkqF2l/+XmWpsSoJ/aenjNwNTT1X672Pfh+jsfL1W8VsiFINnDuLiiy8u9iS39NJLd0444YSJO6MHtu1MbPxuYe6stdZa5Vc7o4quWbMwHyZzfiXUbah3rr/++oknhoM5O2WBPXj00UeXHRt0/dV9x6MGcZ3OXJ1xgnQZuNKTU0P7ynpaywunmzYyka/erQbJ74img3auzghgLhO/XyAH3c8m5dmNw49DgFyOOeaYskprOpg3vTotxhf+kq5zwjQNP8/QHvB/XNcKxEEHHTQRcupoid9ipEGbqwFtQWIAz5JFexKZkmIev2kQ1u9OFy3xW4w0tAECm45ZlqlXJ5hp705L/BYjDdqeOcNN1s7RPpoJWuK3GFvYk4eZM5MR+Jb4AwINZXidRorG6nXpzWGzZnsRWws6N7++fr7F/4eJhmAAbLpouzMHBKQ1RG+0kQvZgV2K0OYP6ZazUS7Ca7AZtbSnjvlENtDKdhy1Xdvif0sX7SdqNwrTtafb3dsSf0DoJX4vEN/ELWTXY2GOukEZhcI2KqZVWJBt+q6/ns/3fvxe7LnnnmXLeEsxd9pppwnfqaM1dQYEREX49DpE4/Pn9EWbvGWNLFvVznFMHXNa2K3mFvlxhJqgxa1hZwZzsGw7Yre5maAl/oCA3EjviPA0fJyFI2x4uyKospHbAow8p6ZQAC666KLm0ksvLWFb3AKL7Mlkhx12KNOcZ4KW+ANArMde4ueI5BqwNHtN6jo88ttez59CMjzf4n8wbYGc0rgN1J7m/OswWBxa4g8ANdG52PkyC6yVlTmW2oHGKwjrHn/nagbV+rgsLRwWtIfyb7TATmx2XrOzHKWxOLTEHxAQVwM3oM1DfhPR7BvDRoUQm38KgfD2C9Ww1dhtcWtEiYBfCNlmRRtp//33XyjX28LIEh9xVPUSxdGg44JkSh1nfhxC20XY9ng2Q0VqaQUFgLYSzn6Y5qEYnMlemS1uAfOPdn/LW95Sfgl71VVXlX2F9thjj4kQi0FX6COHrv3bWWeddbBhodtss80m7o4HugTudM2ZTtd0KQtJutp/oXPdtfM7J554YmfLLbfsrL766p1ll1220yV7cd1C0Vl77bU7u+yyS+ecc87p3HzzzRNvbQFdk+ZW3FhqqaU6BxxwwLQWn49kP74NRP0QgDlgadrnPve54m+E006644D048fEIWYuWt/R1uD68C2H1Hujwese29U+OeapmI6rdmhxC3QO+HGc9g95mbWpdpwOxmIAy0CORQgGePwDtReSgGijNLq5OLEivjDMG7ap3h0FhYljt2Ari5hEwnEt+ouxb9zqEvRPLIVjlBDCLsoljMKqL5ot7zemNBcNr6GbcGOgm8YOY018NYDfBBnoQf5xA0KrqpGcluc0ZO0kwN99tcA4NezHBWNLfPNcbBti9HPnnXcuW+qNI2h1bYBsm+Ea0eNabT8YjAXx/RcLkAIR7B/pp2AGMkxJ9fc8GnLcID0xZwLp014J6RWKcUzbyKMr3JHGpZde2umSo7Paaqt1rr766s4mm2xCBXa6ZkHnlFNOmQg1N9Ale+kG1QXqmK5PrkV/MfK9On564I8fK6+8cvlXkq4sXZ0LFixoVllllYlQ4wmanfhp9Gj/3uzorRFa9AcjTfwsNgg0/mwBvuuuu86JhRkarsSP3DX5gX9c7rfoH0Zamueff/7EWdO86U1vKv9F9ce8ubIaqSY6hOgtBo+R1vgaeH4RycwZ5S0AlxTJAulttftwMBYjt3MdvVlQ1wItBoGm+T/mdhoJEJO9owAAAABJRU5ErkJggg==)

tételek amiket használok.
 
### Egy **vonal** a gráf **Euler-vonala** ha a gráf minden élét tartalmazza, lehet zárt vagy nyílt( a ceruza felemelése nélkül le lehet rajzolni a gráfot).
### **Zárt Euler-vonal** ha a gráf minden csúcsának fokszáma **páros**.
### **Nyílt Euler-vonal** ha a gráfnak **két paratlan** és **n-2 páros** fokszámú csúcsa

Szóval megszámoljuk a gráf minden csúcsának a fokszzámát és ha valamelyik feltétel teljesül akkor le lehet rajzolni a gráfot ceruza felemelése nélkül. Ennek a gráfnak 2 páratlan fokszámú csúcsa van és az összes többi páros tehát van benne Nyílt Euler vonal, szóval le lehet rajzolni a ceruza felemelése nélkül.
